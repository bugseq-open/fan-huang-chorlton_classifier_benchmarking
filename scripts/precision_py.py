"""
Instructions
1) Run ncbi = NCBITaxa() to ensure that a local copy of NCBI taxononomy 
files are saved in home directory (in the format ete toolkit needs). 
2) Save classification file 'Zymo-GridION-LOG-BB-SN-flipflop.fq.tsv.rcf.data.tsv'
and 'expected_microbes.csv' in home directory. 
3) Run <python precision.py>

4) Results will look like below: 
"""

# Imports
import pandas as pd
import numpy as np
from ete3 import NCBITaxa
pd.options.display.max_rows = 999

# Define class instance
ncbi = NCBITaxa()

# Define list needed later
ordered_levels = ['superkingdom', 'phylum', 'order', 'family',
                  'genus', 'species']
        
# Read in selected columns of data, rename some columns.
seq_file='Zymo-GridION-EVEN-BB-SN.fq.tsv.rcf.data_even_N10.tsv'
#seq_file='Zymo-GridION-LOG-BB-SN-flipflop.fq.tsv.rcf.data_log_N10.tsv'
#seq_file='Zymo-GridION-EVEN-BB-SN.fq.tsv.rcf.data_even_N25.tsv'
#seq_file='Zymo-GridION-LOG-BB-SN-flipflop.fq.tsv.rcf.data_log_N25.tsv'

#seq_file='zymoEven_centrifuge_minhit16.class.rcf.data_even.tsv'
#seq_file='zymoLog_centrifuge_minhit16.class.rcf.data_log.tsv'
#seq_file='zymoEven_centrifuge_minhit22.class.rcf.data_even.tsv'
#seq_file='zymoLog_centrifuge_minhit22.class.rcf.data_log.tsv'




data = pd.read_csv(seq_file, sep='\t',
                   skiprows=[0, 2], usecols=['Stats', 'count', 'Rank', 'Name'])
data.columns=['taxid', 'count', 'rank', 'name']
expected=pd.read_csv('expected_microbes.csv', index_col=0)

# Get dataframes subsetted at different taxonomic levels from 'ordered_levels'.
# Also get (sum of counts of expected microbes)*100/(sum of all counts) at 
# different taxonomic levels from 'ordered_levels'.
new_df={}
new_df2={}
precision={}
for val in ordered_levels:
    df1=data[data['rank']==val]
    df2=df1[df1['taxid'].isin(expected[val])]
    new_df2[val]=df2['name'].values
    new_df[val]=df1
    precision[val]=df2['count'].sum()*100/df1['count'].sum()
    print(df1['count'].sum())
    print(df2['count'].sum())

# Display results
for val in ordered_levels:
    #print(new_df[val])
	#print(new_df[val][new_df[val].taxid.isin(expected[val])])
    print('\nPrecision at {} level is {} %'.format(val,round(precision[val],4)))
    print('Expected {}: {}'.format(val,new_df2[val]))